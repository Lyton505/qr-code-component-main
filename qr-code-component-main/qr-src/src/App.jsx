import { useState } from "react";
import "./App.css";
import qrLogo from "/home/lyton/Documents/frontend-mentor/qr-code-component-main/qr-src/public/image-qr-code.png";

function App() {
  const [count, setCount] = useState(0);

  return (
    <>
      <div className={"outer"}>
        <div className={"qr-cont"}>
          <a href="https://lyton.dev" target={"_blank"}>
            <img src={qrLogo} alt="QR code" />
          </a>
        </div>
        <div className={"subheader"}>
          Improve your front-end skills by building projects
        </div>
        <div className={"scantxt"}>
          Scan the QR code to visit Frontend Mentor and take your coding skills
          to the next level
        </div>
      </div>
    </>
  );
}

export default App;
